package org.gobiiproject.gobiimodel.types;

public class BrapiDefaults {

    public static final int pageSize = 1000;
    public static final String pageSizeStr = "1000";
}
