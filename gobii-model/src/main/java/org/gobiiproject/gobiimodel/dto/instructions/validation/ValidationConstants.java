package org.gobiiproject.gobiimodel.dto.instructions.validation;

public class ValidationConstants {
    public static final String CV = "CV";
    public static final String REFERENCE = "reference";
    public static final String LINKAGE_GROUP = "linkage_group";
    public static final String EXTERNAL_CODE ="external_code";
    public static final String DNARUN = "dnarun";
    public static final String MARKER = "marker";
    public static final String DNASAMPLE = "dnasample";
    public static final String DNASAMPLE_NAME = "dnasample_name";
    public static final String DNASAMPLE_NAME_NUM = "sampleNameNum";
    public static final String DB = "DB";
    public static final String FILE = "FILE";
    public static final String FILE_SUBSET = "FILE_SUBSET";
    public static final String YES = "YES";
    public static final String NO = "NO";
    public static final String SUCCESS = "PASSED";
    public static final String FAILURE = "FAILED";
}
