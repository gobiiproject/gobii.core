package org.gobiiproject.gobiiprocess.digester;

import lombok.Data;

@Data
public class MatrixProcessorResult extends ProcessorResult {

	private Integer numRows;
	private Integer numColumns;

}
