
Retrieves the Project entity having the specified ID.

**Response Body**

Field | Type | Description
------|------|------------
result | Object | [Project Resource](https://gdmsampletracking.docs.apiary.io/#reference/projects)


